<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models\Team;

class InscritsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $teams = Team::orderBy('created_at', 'DESC')->get();
        return view('inscrits', [
            'teams' => $teams
        ]);
    }
}
