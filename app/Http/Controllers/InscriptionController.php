<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models\Team;
use Illuminate\Http\Request;

class InscriptionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('inscription');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function inscription(Request $request, $id = null)
    {
        if($id == null) {
            $team = Team::create();
            for ($i = 0; $i < $request->get('team_size'); $i++) {
                $runner = $team->runners()->create([
                    'firstName' => $request->get('first_name')[$i],
                    'lastName' => $request->get('last_name')[$i],
                    'email' => $request->get('email')[$i],
                    'phone' => $request->get('telephone')[$i],
                    'city' => $request->get('city')[$i]
                ]);
                $runners[] = $runner;
            }
        } else {
            $team = Team::find($id);
        }
        $request->session()->set('team', $team->id);

        return view('payment', [
            'team' => $team
        ]);
    }


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function charge(Request $request)
    {

        /** @var Team $team */
        $team = Team::find($request->session()->get('team'));

        $email = $request->get('stripeEmail');

        try {
            $team->createAsStripeCustomer($request->input('stripeToken'));
            if(!$team->charge(2000, [
                'currency' => 'eur',
                'description' => $email
            ])){
                throw new Exception('Your card was declined');
            }
            $beautymail = app()->make(\Snowfire\Beautymail\Beautymail::class);
            $beautymail->send('emails.success', [], function ($message) use ($email) {
                $message
                    ->from('passe.ton.delire@gmail.com')
                    ->to($email)
                    ->subject('Passe ton délire');
            });
            $team->paid = true;
            $team->save();
            $request->session()->remove('team');
        } catch (\Exception $e) {
            $beautymail = app()->make(\Snowfire\Beautymail\Beautymail::class);
            $beautymail->send('emails.failure', ['team_id' => $team->id], function ($message) use ($email) {
                $message
                    ->from('passe.ton.delire@gmail.com')
                    ->to($email)
                    ->subject('Passe ton délire');
            });
            return view('message', [
                'title' => 'Inscription échouée',
                'message' => 'Une erreur a eu lieu lors de paiement, rendez-vous <a href="https://passe-ton-delire.cu.cc/inscription/'.$team->id.'">ici</a> pour payer et valider l\'inscription'
            ]);
        }

        return view('message', [
            'title' => 'Inscription validée',
            'message' => 'L\'inscription a bien été validée, vous aller recevoir un mail de confirmation.'
        ]);
    }


}
