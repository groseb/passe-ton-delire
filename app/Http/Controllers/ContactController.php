<?php

namespace App\Http\Controllers;

use App\Http\Requests;

class ContactController extends Controller
{
    /**
     * Show the contact form
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('contact', [
            'status' => 'form'
        ]);
    }

    public function send(Requests\ContactFormRequest $request)
    {
        $email = $request->get('email');
        $beautymail = app()->make(\Snowfire\Beautymail\Beautymail::class);
        $beautymail->send('emails.contact', array(
            'name' => $request->get('name'),
            'email' => $email,
            'user_message' => $request->get('message')
        ), function ($message) use ($email) {
            $message
                ->from('passe.ton.delire@gmail.com')
                ->to('passe.ton.delire@gmail.com')
                ->subject('Formulaire de contact');
        });

        return redirect('contact')->with('status', 'success');
    }
}
