<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/inscrits', 'InscritsController@index');
Route::get('/inscription', 'InscriptionController@index');
Route::get('/inscription/{id}', 'InscriptionController@inscription');
Route::post('/inscription', 'InscriptionController@inscription');
Route::post('/charge', 'InscriptionController@charge');

//Route::auth();
Route::get('login', 'Auth\AuthController@showLoginForm');
Route::post('login', 'Auth\AuthController@login');
Route::get('logout', 'Auth\AuthController@logout');
Route::get('password/reset/{token?}', 'Auth\PasswordController@showResetForm');
Route::post('password/email', 'Auth\PasswordController@sendResetLinkEmail');
Route::post('password/reset', 'Auth\PasswordController@reset');

Route::get('/home', 'HomeController@index');
Route::get('/photos', 'InformationsController@photos');
Route::get('/informations', 'InformationsController@index');
Route::get('/partenaires', 'InformationsController@partenaires');
Route::get('/contact', 'ContactController@index')->name('contact');;
Route::post('/contact', 'ContactController@send')->name('contact_post');;
