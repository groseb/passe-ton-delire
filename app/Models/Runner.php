<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Runner extends Model
{

    protected $table = 'runner';
    public $timestamps = false;

    protected $fillable = ['firstName', 'lastName', 'email', 'phone', 'city'];

    public function team() {
        return $this->belongsTo('App\Models\Team');
    }

}