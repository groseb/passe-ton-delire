<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Laravel\Cashier\Billable;

class Team extends Model
{
    use Billable;

    protected $table = 'team';
    public $timestamps = true;

    protected $fillable = ['paid'];

    public function runners()
    {
        return $this->hasMany('App\Models\Runner');
    }
}