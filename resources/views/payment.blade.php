@extends('layouts.app')

@section('title', 'Inscription')

@section('content')
    <div class="container">
        <div class="section">
            <div class="row">
                <form class="col s12" action="{{ url('/charge') }}" method="POST">
                    <h3>Récapitulatif de votre inscription</h3>
                    {!! csrf_field() !!}

                    <?php /** @var $team \App\Models\Team */ ?>

                    <ul class="collection">
                        <li class="collection-item">Participants :<br>
                            <ul style="padding-left: 40px;">
                                @foreach($team->runners as $runner)
                                    <li style="list-style-type: circle;">{{ $runner->firstName }} {{ $runner->lastName }}</li>
                                @endforeach
                            </ul>
                        </li>
                        <li class="collection-item">Nombre de membres dans l'équipe : {{ count($team->runners) }}</li>
                        <li class="collection-item">Adresse email : {{ $team->runners()->first()->email }}</li>
                    </ul>

                    @if($team->paid == true)

                        <div class="row">
                            <div class="col s12 m5">
                                <div class="card-panel teal">
                                    <span class="white-text">Le paiement pour cette équipe a déjà été validé !
                                    </span>
                                </div>
                            </div>
                        </div>

                    @else

                        <script
                                src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                                data-key="pk_live_G1cCeykBwCF9Se3NLHXnSUMU"
                                data-name="Passe ton délire"
                                data-email="{{ $team->runners()->first()->email }}"
                                data-description="Inscription d'une équipe"
                                data-zip-code="true"
                                data-currency="EUR"
                                data-amount="2000"
                                data-label="Payer pour valider"
                                data-locale="auto">
                        </script>

                    @endif
                </form>
            </div>
        </div>
    </div>
@endsection