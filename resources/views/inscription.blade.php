@extends('layouts.app')

@section('title', 'Inscription')

@section('content')
    <div class="container">
        <div class="section">
            <div class="row">
                @if (1 == 2)
                <div class="col s12">
                    Inscriptions closes
                </div>
                @else
                    <form class="col s12" method="POST">
                        <h3>Inscription</h3>
                        {!! csrf_field() !!}

                        <div class="row">
                            <div class="col s12">
                                Il faut être agé de 16 ans au minimum pour participer à la course.<br>
                                Pour chaque participant mineur, merci de nous transmettre une
                                <a target="_blank" href="{{ url('/autorisation_parentale.pdf') }}">autorisation parentale</a>
                                complétée et signée à l'adresse <a href="mailto:passe.ton.delire@gmail.com">passe.ton.delire@gmail.com</a>.
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field col s12">
                                <select id="team_size" name="team_size" required="required">
                                    <option>2</option>
                                    <option selected="selected">4</option>
                                </select>
                                <label for="team_size">Nombre de participants</label>
                            </div>
                        </div>
                        <ul class="collection">
                            <li class="collection-item" id="participant1">
                                <span class="title">Participant 1</span>
                                <div class="row">
                                    <div class="input-field col s6">
                                        <input id="first_name" name="first_name[0]" type="text" class="validate"
                                               required="required">
                                        <label for="first_name">Prénom</label>
                                    </div>
                                    <div class="input-field col s6">
                                        <input id="last_name" name="last_name[0]" type="text" class="validate"
                                               required="required">
                                        <label for="last_name">Nom</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s6">
                                        <input id="email" name="email[0]" type="email" class="validate" required="required">
                                        <label for="email">Email</label>
                                    </div>
                                    <div class="input-field col s6">
                                        <input id="telephone" name="telephone[0]" type="text" class="validate"
                                               required="required">
                                        <label for="telephone">Téléphone</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12">
                                        <input id="city" name="city[0]" type="text" class="validate" required="required">
                                        <label for="city">Ville</label>
                                    </div>
                                </div>
                            </li>
                            <li class="collection-item" id="participant2">
                                <span class="title">Participant 2</span>
                                <div class="row">
                                    <div class="input-field col s6">
                                        <input id="first_name" name="first_name[1]" type="text" class="validate"
                                               required="required">
                                        <label for="first_name">Prénom</label>
                                    </div>
                                    <div class="input-field col s6">
                                        <input id="last_name" name="last_name[1]" type="text" class="validate"
                                               required="required">
                                        <label for="last_name">Nom</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s6">
                                        <input id="email" name="email[1]" type="email" class="validate" required="required">
                                        <label for="email">Email</label>
                                    </div>
                                    <div class="input-field col s6">
                                        <input id="telephone" name="telephone[1]" type="text" class="validate"
                                               required="required">
                                        <label for="telephone">Téléphone</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12">
                                        <input id="city" name="city[1]" type="text" class="validate" required="required">
                                        <label for="city">Ville</label>
                                    </div>
                                </div>
                            </li>

                            <li class="collection-item" id="participant3">
                                <span class="title">Participant 3</span>
                                <div class="row">
                                    <div class="input-field col s6">
                                        <input id="first_name" name="first_name[2]" type="text" class="validate"
                                               required="required">
                                        <label for="first_name">Prénom</label>
                                    </div>
                                    <div class="input-field col s6">
                                        <input id="last_name" name="last_name[2]" type="text" class="validate"
                                               required="required">
                                        <label for="last_name">Nom</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s6">
                                        <input id="email" name="email[2]" type="email" class="validate" required="required">
                                        <label for="email">Email</label>
                                    </div>
                                    <div class="input-field col s6">
                                        <input id="telephone" name="telephone[2]" type="text" class="validate"
                                               required="required">
                                        <label for="telephone">Téléphone</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12">
                                        <input id="city" name="city[2]" type="text" class="validate" required="required">
                                        <label for="city">Ville</label>
                                    </div>
                                </div>
                            </li>
                            <li class="collection-item" id="participant4">
                                <span class="title">Participant 4</span>
                                <div class="row">
                                    <div class="input-field col s6">
                                        <input id="first_name" name="first_name[3]" type="text" class="validate"
                                               required="required">
                                        <label for="first_name">Prénom</label>
                                    </div>
                                    <div class="input-field col s6">
                                        <input id="last_name" name="last_name[3]" type="text" class="validate"
                                               required="required">
                                        <label for="last_name">Nom</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s6">
                                        <input id="email" name="email[3]" type="email" class="validate" required="required">
                                        <label for="email">Email</label>
                                    </div>
                                    <div class="input-field col s6">
                                        <input id="telephone" name="telephone[3]" type="text" class="validate"
                                               required="required">
                                        <label for="telephone">Téléphone</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12">
                                        <input id="city" name="city[3]" type="text" class="validate" required="required">
                                        <label for="city">Ville</label>
                                    </div>
                                </div>
                            </li>
                        </ul>

                        <label>En cliquant sur valider vous acceptez le
                            <a target="_blank" href="{{ url('/reglement.pdf') }}">reglement</a>
                            et déclarez disposer d'une assurance de responsabilité civile
                        </label>

                        <button class="btn waves-effect waves-light" type="submit" name="action">Valider
                            <i class="material-icons right">send</i>
                        </button>
                    </form>
                @endif
            </div>
        </div>
    </div>
@endsection

@section('endpage_js')
    <script>
        $(function () {
            $('#team_size').on('change', function () {
                if ($(this).val() == 2) {
                    $('#participant3').hide();
                    $('#participant3').find('*').attr('disabled', true);
                    $('#participant4').hide();
                    $('#participant4').find('*').attr('disabled', true);
                } else {
                    $('#participant3').show();
                    $('#participant3').find('*').attr('disabled', false);
                    $('#participant4').show();
                    $('#participant4').find('*').attr('disabled', false);
                }
            });
            var teamSize = document.getElementById("team_size");
            var event = new Event('change');
            teamSize.dispatchEvent(event);
        });
    </script>
@endsection