<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
    <title>Passe ton délire - @yield('title')</title>

    <!-- CSS  -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Bangers' rel='stylesheet' type='text/css'>
    <link href="//cdnjs.cloudflare.com/ajax/libs/materialize/0.97.6/css/materialize.min.css" type="text/css"
          rel="stylesheet" media="screen,projection"/>
    <link href="{{ url('css/style.css') }}" type="text/css" rel="stylesheet" media="screen,projection"/>
    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                        (i[r].q = i[r].q || []).push(arguments)
                    }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                    m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-43865328-10', 'auto');
        ga('send', 'pageview');
    </script>
</head>
<body>
<ul id="usermenu" class="dropdown-content">
    <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
</ul>
<nav class="white" role="navigation">
    <div class="nav-wrapper container">
        <a id="logo-container" href="#" class="brand-logo"></a>
        <ul class="right hide-on-med-and-down">
            <li><a href="{{ url('/') }}">Accueil</a></li>
            <li><a href="{{ url('/photos') }}">Photos</a></li>
            <li><a href="{{ url('/inscription') }}">S'inscrire</a></li>
            <li><a href="{{ route('contact') }}">Contact</a></li>
            @if (Auth::guest())
                <!-- Authentication Links <li><a href="{{ url('/login') }}">Login</a></li>
                <li><a href="{{ url('/register') }}">Register</a></li> -->
            @else
            <li><a href="{{ url('/inscrits') }}">Équipes inscrites</a></li>
            <li><a class="dropdown-button" href="#!" data-activates="usermenu">{{ Auth::user()->name }} <i class="material-icons right">arrow_drop_down</i></a></li>
            @endif
        </ul>

        <ul id="nav-mobile" class="side-nav" style="left: 0;">
            <li><a href="{{ url('/') }}">Accueil</a></li>
            <li><a href="{{ url('/photos') }}">Photos</a></li>
            <li><a href="{{ url('/inscription') }}">S'inscrire</a></li>
            @if (Auth::guest())
                <!-- Authentication Links<li><a href="{{ url('/login') }}">Login</a></li>
                <li><a href="{{ url('/register') }}">Register</a></li> -->
            @else
            <li><a href="{{ url('/inscrits') }}">Équipes inscrites</a></li>
            <li><a class="dropdown-button" href="#!" data-activates="dropdown1">{{ Auth::user()->name }} <i class="material-icons right">arrow_drop_down</i></a></li>
            @endif
        </ul>
        <a href="#" data-activates="nav-mobile" class="button-collapse"><i class="material-icons">menu</i></a>
    </div>
</nav>

@yield('content')

<footer class="page-footer orange darken-3">
    <div class="container">
        <div class="row">
            <div class="col l6 s12">
                <div class="row">
                    <div class="col l6 s12">
                        <div class="card-panel grey lighten-5 z-depth-1">
                            <img class="responsive-img" src="{{ url('/img/logo_bpa.png') }}"/>
                        </div>
                    </div>
                    <div class="col l6 s12">
                        <div class="card-panel grey lighten-5 z-depth-1">
                            <img class="responsive-img" src="{{ url('/img/logo_axa.png') }}" style="width:262px; height:84px;"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col l3 s12">
                <h5 class="white-text">Menu</h5>
                <ul>
                    <li><a class="white-text" href="{{ url('/') }}">Accueil</a></li>
                    <li><a class="white-text" href="{{ url('/inscription') }}">Inscription</a></li>
                    <li><a class="white-text" href="{{ url('/photos') }}">Photos</a></li>
                </ul>
            </div>
            <div class="col l3 s12">
                <h5 class="white-text">Partenaires</h5>
                <ul>
                    <li><a class="white-text" target="_blank" href="http://www.jce-chateaubriant.com/">JCE
                            Châteaubriant</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="footer-copyright">
        <!--<div class="container">
            Made by <a class="brown-text text-lighten-3" href="http://materializecss.com">Materialize</a>
        </div>-->
    </div>
</footer>


<!--  Scripts-->
<script src="//code.jquery.com/jquery-2.1.1.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/materialize/0.97.6/js/materialize.min.js"></script>
<script src="{{ url('js/init.js') }}"></script>
<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
{{-- <script src="{{ elixir('js/app.js') }}"></script> --}}
@yield('endpage_js')
</body>
</html>
