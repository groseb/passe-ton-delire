@extends('layouts.app')

@section('title', 'Accueil')

@section('sidebar')
    @parent

    <nav class="white" role="navigation">
        <div class="nav-wrapper container">
            <a id="logo-container" href="#" class="brand-logo">Logo</a>
            <ul class="right hide-on-med-and-down">
                <li><a href="#">Navbar Link</a></li>
            </ul>

            <ul id="nav-mobile" class="side-nav">
                <li><a href="#">Navbar Link</a></li>
            </ul>
            <a href="#" data-activates="nav-mobile" class="button-collapse"><i class="material-icons">menu</i></a>
        </div>
    </nav>
@endsection

@section('content')
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <style>
        body {
            background: url(img/background/PTD_Fond2Site.jpg) no-repeat center center;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
            background-position-y: -300px;
            background-size: 100% 70%;
        }
        .flow-text {
            font-weight: 400;
            margin: 12px;
            color: white;
        }
    </style>

    <div class="container" style="margin-top: 10px;">
        <div class="section">
        <div class="row">
            <div class="s12 center">
                <img class="responsive-img" style="width: 100%;" src="img/background/background1.png" alt="Avengers">
            </div>
            </div>
        </div>
    </div>


    <div class="container">
        <div class="section">
            <div class="row">
                <div class="col l12 s12 center">
                    <blockquote style="font-family: 'Bangers', cursive; font-size: x-large;">
                        <p>Chers héros intrépides,</p>
                        Préparez-vous à re-sortir vos super-pouvoirs pour tenter de remporter <br>
                        la 2e édition de "Passe ton délire.... si tu peux" le dimanche 8 octobre prochain.<br>
                        Mais pour cela, oserez-vous relever les nouveaux défis proposés<br>
                        en enfilant la tenue que nous vous fournirons ?<br>
                        Nous sommes impatients de vous voir à l'épreuve,<br>
                        alors <a href="{{ url('/inscription') }}">inscrivez-vous</a> vite sur notre site !<br>
                        <br>
                        Retrouvez-nous sur notre page <a style="font-family: 'Roboto', sans-serif; font-weight: 900; padding: 4px 7px 4px 8px; background-color: #3b5998; color: white;">facebook</a> !
                    </blockquote>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="section">
            <div class="row">
                <div class="col l6 s12 center">
                    <h3><i class="mdi-content-send brown-text"></i></h3>
                    <h4 style="color: white; font-weight: 600;">Informations</h4>

                    <p class="flow-text">Le dimanche 8 Octobre 2017 à 9H15</p>
                    <p class="flow-text">Rendez-vous sur l'esplanade du château à Châteaubriant</p>
                    <p class="flow-text">Course en relais par équipe de 2 ou 4</p>
                    <p class="flow-text">Course de 10kms à effectuer en équipe (4x2.5kms ou 2x5kms) avec épreuves
                        individuelles</p>
                    <p class="flow-text">Inscription à 20€ par équipe</p>
                    <p class="flow-text">Des déguisements vous seront fournis !</p>
                </div>
                <div class="col l6 s12 center">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3320.3156960409738!2d-1.373365984003584!3d47.719111279192354!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x480f4afe8e4d3359%3A0x2053c4b7adca6f31!2sEspl.+du+Ch%C3%A2teau%2C+44110+Ch%C3%A2teaubriant!5e1!3m2!1sfr!2sfr!4v1462899164186"
                            width="100%" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="section">
            <div class="row">
                <div class="col s12 center">
                    <iframe width="100%" height="500" src="https://www.youtube.com/embed/kHgfa5pQkuY" frameborder="0"
                            allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="section">
            <div class="row valign-wrapper">
                <div class="col hide-on-small-only m3">
                    <img class="img-responsive" src="{{ url('img/Logo-JCE.jpg') }}" style="width: 100%;" alt="JCI"/>
                </div>
                <div class="col s12 m9">
                    <h3><i class="mdi-content-send brown-text"></i></h3>
                    <h4>Qui sommes-nous ?</h4>
                    <p class="left-align light">Des citoyens engagés, tous bénévoles, âgés de 18 à 40 ans, qui ont envie
                        de donner un peu de leur temps et de leur énergie au service de projets innovants et utiles à
                        notre territoire.
                        Tout au long de l'année, nous proposons à nos membres de nombreux moments de rencontre pour
                        travailler sur des projets en équipe, se former à la prise de responsabilités, mais surtout
                        passer de bons moments dans un cadre convivial et détendu.
                        Nous vous invitons vivement à découvrir <a href="http://www.jce-chateaubriant.com/">notre site
                            web</a>,
                        qui vous aidera à mieux comprendre ce qu'est la Jeune Chambre Économique.
                        Vous pourrez ainsi vous rendre compte de nos envies de
                        partage, de convivialité, d'expériences enrichissantes.
                        La vie est faite d'engagements. Le nôtre : Faire progresser la société en progressant
                        individuellement.</p>
                </div>
            </div>
        </div>
    </div>

    <div class="parallax-container valign-wrapper">
        <div class="section no-pad-bot">
            <div class="container">
                <div class="row center">
                    <!--<h5 class="header col s12 light">A modern responsive front-end framework based on Material
                        Design</h5>-->
                </div>
            </div>
        </div>
        <div class="parallax"><img src="{{ url('img/background/background2.jpg') }}" alt="Unsplashed background img 2">
        </div>
    </div>

    <!--<div class="parallax-container valign-wrapper">
    <div class="section no-pad-bot">
        <div class="container">
            <div class="row center">
                <h5 class="header col s12 light">Participe à une course décalée !</h5>
            </div>
        </div>
    </div>
    <div class="parallax"><img src="{{ url('img/background/background3.jpg') }}" alt="Unsplashed background img 3"></div>
</div>-->


    <div class="container">
        <div class="section">
            <div class="row">
                <div class="col s12 center">
                    <h3><i class="mdi-content-send brown-text"></i></h3>
                    <h4>Partenaires</h4>
                    Nous remercions tous nos partenaires pour leur soutien et sans qui cette course n'aurait pas pu
                    avoir lieu !
                    <div class="row">
                        <div class="col s6 offset-s0 m3 offset-m3">
                            <img class="img-responsive" src="{{ url('img/logo_marmite.jpg') }}" style="width: 100%;"
                                 alt="Marmite du meunier"/>
                        </div>
                        <div class="col s6 m3 pull-m0">
                            <img class="img-responsive" src="{{ url('img/logo_vb.png') }}" style="width: 100%;"
                                 alt="V&B Châteaubriant"/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col s6 offset-s0 m3 offset-m3">
                            <img class="img-responsive" src="{{ url('img/Lgoo_Leclerc.png') }}" style="width: 100%;"
                                 alt="Leclerc"/>
                        </div>
                        <div class="col s6 m3">
                            <img class="img-responsive" src="{{ url('img/logo_jardiland.png') }}" style="width: 100%;"
                                 alt="Jardiland"/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col s6 offset-s0 m3 offset-m3">
                            <img class="img-responsive" src="{{ url('img/Logo_Systeme-coiffure.png') }}" style="width: 100%;"
                                 alt="Systeme-coiffure"/>
                        </div>
                        <div class="col s6 m3">
                            <img class="img-responsive" src="{{ url('img/logo_Sobhi-Ancenis.png') }}" style="width: 100%;"
                                 alt="Sobhi-Ancenis"/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col s4 m2 offset-m3">
                            <img class="img-responsive" src="{{ url('img/lencre-services_Logo.gif') }}"
                                 style="width: 100%;" alt="L'Encre"/>
                        </div>
                        <div class="col s4 m2">
                            <img class="img-responsive" src="{{ url('img/logo_bollore-mainguet.png') }}"
                                 style="width: 100%;" alt="Bolloré et Mainguet"/>
                        </div>
                        <div class="col s4 m2">
                            <img class="img-responsive" src="{{ url('img/logo_Artis.jpg') }}" style="width: 100%;"
                                 alt="Artis"/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col s4 m2 offset-m3">
                            <img class="img-responsive" src="{{ url('img/Logo_phila-coiffure.png') }}"
                                 style="width: 100%;" alt="phila-coiffure"/>
                        </div>
                        <div class="col s4 m2">
                            <img class="img-responsive" src="{{ url('img/Logo_Garage-Fromentin.png') }}"
                                 style="width: 100%;" alt="Garage-Fromentin"/>
                        </div>
                        <div class="col s4 m2">
                            <img class="img-responsive" src="{{ url('img/Logo-CIC.png') }}" style="width: 100%;"
                                 alt="CIC"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--
    <div class="container">
        <div class="section">
            <div class="row">
                <div class="col s12 center">
                    <h3><i class="mdi-content-send brown-text"></i></h3>
                    <h4>Contactez-nous</h4>
                    <p class="left-align light">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam
                        scelerisque id nunc nec volutpat. Etiam pellentesque tristique arcu, non consequat magna
                        fermentum ac. Cras ut ultricies eros. Maecenas eros justo, ullamcorper a sapien id, viverra
                        ultrices eros. Morbi sem neque, posuere et pretium eget, bibendum sollicitudin lacus. Aliquam
                        eleifend sollicitudin diam, eu mattis nisl maximus sed. Nulla imperdiet semper molestie. Morbi
                        massa odio, condimentum sed ipsum ac, gravida ultrices erat. Nullam eget dignissim mauris, non
                        tristique erat. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia
                        Curae;</p>
                </div>
            </div>
        </div>
    </div>
    -->

@endsection