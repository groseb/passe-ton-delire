@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col s12">
                <h1>Contact Passe ton délire</h1>

                @if(session('status') == 'success')
                <div class="card-panel teal">
                    <span class="white-text">Votre message a bien été envoyé !</span>
                </div>
                @else

                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>

                    {!! Form::open(array('route' => 'contact_post', 'class' => 'form row')) !!}

                    <div class="input-field col s12">
                        {!! Form::label('Votre nom') !!}
                        {!! Form::text('name', null,
                        array('required',
                        'class'=>'',
                        'placeholder'=>'Votre nom')) !!}
                    </div>

                    <div class="input-field col s12">
                        {!! Form::label('Votre adresse email') !!}
                        {!! Form::email('email', null,
                        array('required',
                        'class'=>'',
                        'placeholder'=>'Votre adresse email')) !!}
                    </div>

                    <div class="input-field col s12">
                        {!! Form::label('Votre message') !!}
                        {!! Form::textarea('message', null,
                        array('required',
                        'class'=>'materialize-textarea',
                        'placeholder'=>'Votre message')) !!}
                    </div>

                    <div class="input-field col s12">
                        {!! Form::submit('Envoyer !',
                        array('class'=>'btn btn-primary')) !!}
                    </div>
                    {!! Form::close() !!}
                @endif
            </div>
        </div>
    </div>
@endsection