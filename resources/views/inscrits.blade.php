@extends('layouts.app')

@section('title', 'Liste des inscrits')

@section('sidebar')
    @parent

    <nav class="white" role="navigation">
        <div class="nav-wrapper container">
            <a id="logo-container" href="#" class="brand-logo">Logo</a>
            <ul class="right hide-on-med-and-down">
                <li><a href="#">Navbar Link</a></li>
            </ul>

            <ul id="nav-mobile" class="side-nav">
                <li><a href="#">Navbar Link</a></li>
            </ul>
            <a href="#" data-activates="nav-mobile" class="button-collapse"><i class="material-icons">menu</i></a>
        </div>
    </nav>
@endsection

@section('content')
    <div class="container">
        <div class="section">
            <!--   Icon Section   -->
            <div class="row">
                <div class="col s12 m12">
                    <h3>Liste des inscrits</h3>
                    <table class="highlight">
                        <thead>
                        <tr>
                            <th data-field="id">Nom de l'équipe</th>
                            <th data-field="name">Participants</th>
                            <th data-field="name">Paiement</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($teams as $team)
                            <tr>
                                <td>{{ count($team->runners) }}</td>
                                <td>
                                    <ul>
                                        @foreach($team->runners as $runner)
                                            <li>{{ $runner->firstName }} {{ $runner->lastName }} ( {{ $runner->email }} - {{ $runner->phone }})</li>
                                        @endforeach
                                    </ul>
                                </td>
                                <td>
                                    @if ($team->paid)
                                        Validé
                                    @else
                                        En attente
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>
@endsection