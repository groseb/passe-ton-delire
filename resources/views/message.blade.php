@extends('layouts.app')

@section('title', 'Inscription')

@section('content')
    <div class="container">
        <div class="section">
            <div class="row">
                <form class="col s12" method="POST">
                    <h3>{{ $title }}</h3>

                    <p>{!! $message !!}</p>
                </form>
            </div>
        </div>
    </div>
@endsection