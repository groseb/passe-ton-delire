@extends('beautymail::templates.ark')

@section('content')

    @include('beautymail::templates.ark.heading', [
        'heading' => 'Passe ton délire !',
        'level' => 'h1'
    ])

    @include('beautymail::templates.ark.contentStart')

    <h4 class="secondary"><strong>Erreur lors de l'inscription</strong></h4>
    <p>Une erreur a eu lieu lors de paiement, rendez-vous <a href="https://passe-ton-delire.cu.cc/inscription/{{$team_id}}">ici</a> pour payer et valider l'inscription</p>

    @include('beautymail::templates.ark.contentEnd')

@stop