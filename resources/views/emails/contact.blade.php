@extends('beautymail::templates.ark')

@section('content')

    @include('beautymail::templates.ark.heading', [
        'heading' => 'Passe ton délire !',
        'level' => 'h1'
    ])

    @include('beautymail::templates.ark.contentStart')

    <h4 class="secondary"><strong>Demande de contact via le formulaire du site</strong></h4>
    <p>Nom : {{$name}}</p>
    <p>Email : {{$email}}</p>
    <p>Message : {{$user_message}}</p>

    @include('beautymail::templates.ark.contentEnd')

@stop