@extends('beautymail::templates.ark')

@section('content')

    @include('beautymail::templates.ark.heading', [
        'heading' => 'Passe ton délire !',
        'level' => 'h1'
    ])

    @include('beautymail::templates.ark.contentStart')

    <h4 class="secondary"><strong>Inscription validée</strong></h4>
    <p>Votre inscription à la course a bien été validée, merci !</p>

    @include('beautymail::templates.ark.contentEnd')

@stop